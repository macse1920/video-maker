package net.ubtuni.videomaker.controllers;

import java.io.IOException;

import javax.sound.sampled.UnsupportedAudioFileException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.ubtuni.videomaker.services.VideoMakerService;

@RestController
public class VideoController
{
	@Autowired
	private VideoMakerService videoMakerService;

	@RequestMapping(value = "create", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
	public ResponseEntity saveVideo(@RequestParam("url") String url, @RequestParam("filePath") String audioFileLocation,
			@RequestParam("longDuration") long audioDuration) throws IOException, InterruptedException
	{
		return videoMakerService.saveVideo(url, audioFileLocation, audioDuration);
	}

}
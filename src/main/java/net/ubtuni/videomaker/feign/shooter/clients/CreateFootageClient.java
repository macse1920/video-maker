package net.ubtuni.videomaker.feign.shooter.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.ubtuni.videomaker.models.CreateFootageDAO;

@FeignClient(name = "create-footage-client", contextId = "create-footage-client", url = "http://localhost:5012")
public interface CreateFootageClient
{
	@RequestMapping(value = "/footage/create", method = RequestMethod.POST)
	ResponseEntity createFootage(@RequestBody CreateFootageDAO createFootageDAO);
}

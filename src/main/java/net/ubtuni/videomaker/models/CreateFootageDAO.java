package net.ubtuni.videomaker.models;

import lombok.Data;
import lombok.Getter;

@Data
public class CreateFootageDAO
{
	private String articleLink;
	private long duration;
	private String screenshotAccessKey;
	private String videoPath;

}
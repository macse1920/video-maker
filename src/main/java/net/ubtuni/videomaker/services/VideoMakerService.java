package net.ubtuni.videomaker.services;

import static java.lang.String.format;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.probe.FFmpegFormat;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.ubtuni.videomaker.configuration.ScreenshotAPIConfig;
import net.ubtuni.videomaker.feign.shooter.clients.CreateFootageClient;
import net.ubtuni.videomaker.models.CreateFootageDAO;

@Service
@Slf4j
public class VideoMakerService
{

	@Autowired
	private ScreenshotAPIConfig screenshotAPIConfig;

	@Autowired
	private CreateFootageClient createFootageClient;

	private static final String BASE_PATH = "/.tech-news-channel/";

	private static final String MP3_FORMAT = "mp3";
	private static final String MP4_FORMAT = "mp4";
	private static final String FINAL_OUTPUT_FORMAT = "VIDEO_%s";
	private static final String GET_DURATION_COMMAND = "ffmpeg -i %s 2>&1 | grep Duration | awk '{print $2}' | tr -d ,";
	private static final String MERGE_VIDEO_COMMAND = "ffmpeg -i %s -i %s -acodec copy -vcodec copy %s";

	public ResponseEntity saveVideo(final String url, final String audioPath, final long duration) throws IOException, InterruptedException
	{
		final String audioFilePath = buildPath(BASE_PATH, UUID.randomUUID().toString(), MP3_FORMAT);

		final File tempFile = new File(audioPath);

		File audioFile = new File(audioFilePath);

		FileUtils.writeByteArrayToFile(audioFile, FileUtils.readFileToByteArray(tempFile));

		final String expectedFootageSimpleName = UUID.randomUUID().toString();
		final String expectedFootageFullPath = buildPath(BASE_PATH, expectedFootageSimpleName, MP4_FORMAT);

		CreateFootageDAO createFootageDAO = buildFootageRequest(url, duration, expectedFootageFullPath);

		ResponseEntity createFootageResponse = createFootageClient.createFootage(createFootageDAO);

		if (createFootageResponse.getStatusCode().equals(HttpStatus.OK))
		{
			File footageFolder = new File(buildPath(BASE_PATH));

			File videoFootage = Arrays.stream(Objects.requireNonNull(footageFolder.listFiles()))
					.filter(file -> file.getName().contains(expectedFootageSimpleName))
					.findFirst()
					.orElseThrow(() -> new IllegalArgumentException("Could not find footage"));

			final String outputFile = String.format(FINAL_OUTPUT_FORMAT, UUID.randomUUID().toString());
			final String errorStream = createFinalVideo(videoFootage, audioFile, buildPath(BASE_PATH, outputFile, MP4_FORMAT));

			if (!errorStream.isEmpty())
			{
				return ResponseEntity.badRequest().body(errorStream);
			}

			final File createdVideo = new File(buildPath(BASE_PATH, outputFile, MP4_FORMAT));


			if (!createdVideo.getAbsolutePath().isEmpty())
			{
				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.set("x-final-video-path", createdVideo.getAbsolutePath());

				return ResponseEntity.ok().headers(responseHeaders).build();
			}
			else
			{
				return ResponseEntity.badRequest().body(String.format("Created file %s does not exist or is not readable!", createdVideo));
			}

		}
		else
		{
			return ResponseEntity
					.badRequest()
					.body(format("Bad request on creating video creating footage with message :%s", createFootageResponse.getBody()));
		}

	}

	/***
	 * @param audioFile
	 * @return duration it milliseconds
	 * @throws java.io.IOException if file does not exist
	 */
	private long getDuration(final File audioFile) throws IOException, InterruptedException
	{
		FFprobe ffprobe = new FFprobe();
		FFmpegProbeResult probeResult = ffprobe.probe(audioFile.getAbsolutePath());
		FFmpegFormat format = probeResult.getFormat();
		return (long) format.duration;
	}

	private CreateFootageDAO buildFootageRequest(final String url, final long duration, final String expectedFootageFullPath) throws IOException, InterruptedException
	{
		CreateFootageDAO createFootageDAO = new CreateFootageDAO();
		createFootageDAO.setArticleLink(url);
		createFootageDAO.setDuration(duration);
		createFootageDAO.setVideoPath(expectedFootageFullPath);
		createFootageDAO.setScreenshotAccessKey(screenshotAPIConfig.getApiKey());

		return createFootageDAO;
	}

	private String buildPath(final String path, final String fileName, final String extension)
	{
		return format("%s%s%s.%s", System.getProperty("user.home"), path, fileName, extension);
	}

	private String buildPath(final String path)
	{
		return format("%s%s", System.getProperty("user.home"), path);
	}

	private String createFinalVideo(final File videoFootage, final File audioFile, final String path) throws IOException, InterruptedException
	{

		final String command = format(MERGE_VIDEO_COMMAND, audioFile.getPath(), videoFootage.getPath(), path);

		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
		builder.redirectErrorStream(true);

		StringBuilder stringBuilder = new StringBuilder();
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		String line;
		while (true)
		{
			line = r.readLine();
			if (line == null)
			{
				break;
			}
			stringBuilder.append(line);
			log.error(line);
		}
		p.waitFor();

		return stringBuilder.toString().trim();
	}

}